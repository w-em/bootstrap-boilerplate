;(function($, window) {
  'use strict';

  $.plugin('wemCounter', {
    defaults: {
      selector: '[data-counter]',
      finishedClass: 'wem-counter-finished'
    },

    /**
     * Inits the plugin.
     */
    init: function () {
      var me = this;

      me.applyDataAttributes();

      me.registerEvents();
    },

    initMenu: function () {

    },

    /**
     * Registers all events.
     */
    registerEvents: function () {
      var me = this;
      var debounce = function (func, delay) {
        let inDebounce
        return function() {
          const context = this
          const args = arguments
          clearTimeout(inDebounce)
          inDebounce = setTimeout(function () {func.apply(context, args)}, delay)
        }
      }
      me._on($(window), 'scroll', debounce($.proxy(me.onScroll, me), 10));
      me._on($(window), 'resize', debounce($.proxy(me.onResize, me), 10));
    },

    onResize: function () {
      this.checkCounter();
    },

    onScroll: function () {
      this.checkCounter();
    },

    isInViewport: function ($element) {
      var elementTop = $element.offset().top;
      var elementBottom = elementTop + $element.outerHeight();

      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();

      return elementBottom > viewportTop && elementTop < viewportBottom;
    },

    checkCounter: function () {
      var self = this;
      var $el = this.$el
      if ($el.hasClass(self.opts.finishedClass) === false) {
        if (self.isInViewport($el) === false) {
          return false;
        }
        if ($el.data('[data-counter-running]')) {
          return false;
        }
        var options = $.extend({}, $el.data('countToOptions') || {});
        $el.data('[data-counter-running]', 'true')
        $el.countTo(options);
      }
    }
  });
})(jQuery, window);
