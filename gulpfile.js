const gulp = require('gulp');
const gulpIf = require('gulp-if');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const cssmin = require('gulp-cssmin');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const jsImport = require('gulp-js-import');
const sourcemaps = require('gulp-sourcemaps');
const clean = require('gulp-clean');
const isProd = process.env.NODE_ENV === 'prod';
const gulpTwig = require('gulp-twig');
const beautify = require('gulp-jsbeautifier');

function twig () {
  return gulp.src(['src/*.twig'])
    .pipe(gulpTwig())
    .pipe(beautify({
      indent_size: 2
    }))
    .pipe(gulp.dest('dist'))
};

function css () {
  return gulp.src('src/sass/style.scss')
    .pipe(gulpIf(!isProd, sourcemaps.init()))
    .pipe(sass({
      includePaths: ['node_modules']
    }).on('error', sass.logError))
    .pipe(gulpIf(!isProd, sourcemaps.write()))
    .pipe(gulpIf(isProd, cssmin()))
    .pipe(gulp.dest('dist/css/'));
}

function js () {
  return gulp.src([
    'src/js/vendors/*.js',
    'src/js/base/*.js',
    'src/components/**/*.js',
    'src/js/index.js',
  ])
    .pipe(jsImport({
      hideConsole: false
    }))
    .pipe(gulpIf(isProd, uglify()))
    .pipe(concat('all.js'))
    .pipe(gulp.dest('dist/js'));
}

function img () {
  return gulp.src('src/img/*')
    .pipe(gulpIf(isProd, imagemin()))
    .pipe(gulp.dest('dist/img/'));
}

function serve () {
  browserSync.init({
    open: true,
    server: './dist'
  });
}

function browserSyncReload (done) {
  browserSync.reload();
  done();
}


function watchFiles () {
  gulp.watch('src/**/*.twig', gulp.series(twig, browserSyncReload));
  gulp.watch('src/**/*.scss', gulp.series(css, browserSyncReload));
  gulp.watch('src/**/*.js', gulp.series(js, browserSyncReload));
  gulp.watch('src/components/**/*.js', gulp.series(js, browserSyncReload));
  gulp.watch('src/img/**/*.*', gulp.series(img));

  return;
}

function del () {
  return gulp.src('dist/*', { read: false })
    .pipe(clean());
}

exports.css = css;
exports.twig = twig;
exports.js = js;
exports.del = del;
exports.serve = gulp.parallel(twig, css, js, img, watchFiles, serve);
exports.default = gulp.series(del, twig, css, js, img);
